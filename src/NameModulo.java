public class NameModulo {


    public int getModulo(String name) {
        int sum = getNumber(name);
        return sum %2;
    }

    private int getNumber(String name){
        int number=0;
        int[] rer = display(name);
        for (int i = 0; i < name.length() ; i++) {
            number += rer[i];
        }
        return number;
    }

    private int[] display(String word) {
        char[] alfabet = getChars();
        int[] result = new int[word.length()];
//        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < word.length(); i++) {
            for (int j = 0; j < 26; j++) {
                if (Character.toUpperCase(word.charAt(i)) == 'Ą') {
                    result[i]=1;
                    break;
                } else if (Character.toUpperCase(word.charAt(i)) == ' ') {
                    result[i]=0;
                    break;
                } else if (Character.toUpperCase(word.charAt(i)) == 'Ć') {
                    result[i]=3;
                    break;
                } else if (Character.toUpperCase(word.charAt(i)) == 'Ę') {
                    result[i]=5;
                    break;
                } else if (Character.toUpperCase(word.charAt(i)) == 'Ó') {
                    result[i]=1;
                    break;
                } else if (Character.toUpperCase(word.charAt(i)) == 'Ż' || Character.toUpperCase(word.charAt(i)) == 'Ź') {
                    result[i]=26;
                    break;
                } else {
                    if (Character.toUpperCase(word.charAt(i)) == alfabet[j]) {
                        result[i]=j + 1;
                        break;
                    }
                }
            }
        }
        return result;
    }



    private char[] getChars() {
        char[] alfabet = new char[26];
        for (int i = 0; i < 26; i++) {
            alfabet[i] = (char) (65 + i);
        }
        return alfabet;
    }
}
