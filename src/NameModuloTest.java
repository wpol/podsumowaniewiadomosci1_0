import org.junit.Test;

import static org.junit.Assert.*;

public class NameModuloTest {

    @Test
    public void testGetModulo() {

        NameModulo nameModulo = new NameModulo();
        int modulo = nameModulo.getModulo("Wojciech Polowczyk");
        assertEquals(modulo, 0);

    }
}